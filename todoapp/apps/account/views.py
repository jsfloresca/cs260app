from django.shortcuts import render
from django.contrib import messages
from django.contrib.auth import login,authenticate,logout
from django.contrib.auth.models import User
from django.http import HttpResponseRedirect
from django.core.context_processors import csrf
from django.core.urlresolvers import reverse
from todoapp.apps.account.forms import UserAddForm
from django.core.exceptions import ObjectDoesNotExist
from django.http import Http404
import tkinter
import tkinter.filedialog

import csv
import smtplib
import string
import random

attendee_emails = []

def user_login(request):
	if request.method == 'POST':
		username=request.POST.get('login_username')
		password=request.POST.get('login_password')
		next=request.POST.get('next','None')
		user=authenticate(username=username,password=password)

		if not User.objects.filter(username=username).exists() or user is None:
			messages.add_message(request,messages.WARNING,'failed to login.')
			return HttpResponseRedirect(reverse('index'))
		login(request,user)
		if next == 'None':
			return HttpResponseRedirect(reverse('index'))
		return HttpResponseRedirect(next)

def read_csv():

	with open('C:\\Python34\\test.csv', 'rt') as csvfile:
		spamreader = csv.reader(csvfile, delimiter=',', quotechar='|')
		next(spamreader)
		for row in spamreader:
		 	attendee_emails.append(row)

def parse_data():
	for value in attendee_emails:
		pwd = id_generator()
		send_email(value[1], value[0], pwd)
		User.objects.create_user(value[1],value[0], pwd)


def id_generator(size=6, chars=string.ascii_uppercase + string.digits):
	return ''.join(random.choice(chars) for _ in range(size))

def send_email(name, email, pwd ):
	fromaddr = 'cs260projectupd@gmail.com'
	toaddrs  = email
	msg = "Hi  " + name +  "<br/>Your Password is " +  pwd
	username = 'cs260projectupd@gmail.com'
	password = 'project1234'
	server = smtplib.SMTP('smtp.gmail.com:587')
	server.starttls()
	server.login(username,password)
	server.sendmail(fromaddr, toaddrs, msg)
	server.quit()

def user_logout(request):
	logout(request)
	return HttpResponseRedirect(reverse('index'))

def user_open(request):
	filepath = tkinter.filedialog.askopenfilename()
	read_csv()
	parse_data()
	return HttpResponseRedirect(reverse('my_admin',))

def my_admin(request):
	if request.user.is_superuser:
		users=User.objects.all()

		if request.method=='POST':
			form=UserAddForm(request.POST)
			if form.is_valid():
				form.save()
				return HttpResponseRedirect(reverse('my_admin'))

		else:
			form=UserAddForm()
		return render(request,'account/myadmin.html',{'title':'my admin','form':form,'users':users})
	else:
		return HttpResponseRedirect(reverse('index'))


def delete_user(request,user_id):
	if request.user.is_superuser:
		try:
			user=User.objects.get(id=user_id)
		except ObjectDoesNotExist:
			raise Http404
		user.delete()
	return HttpResponseRedirect(reverse('my_admin',))
	



